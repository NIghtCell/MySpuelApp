package com.example.max.myspuelapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.j256.ormlite.android.apptools.OpenHelperManager;

public class AddSpueler extends AppCompatActivity {

    SpuelerDaoImpl spuelerDaoImpl = new SpuelerDaoImpl(this);

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_spueler);
        initListeners();
    }

    private void initListeners() {
        Button button = (Button) findViewById(R.id.add);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validateInputAndCreateSpueler(view);
            }
        });
    }

    private void validateInputAndCreateSpueler(View view) {
        spuelerDaoImpl.initializeDbHelper();
        String name = readNameInput();
        View currentView = getView();
        Validator validator = new Validator();
        if (!name.isEmpty() && !validator.isNameToLong(name)) {
            createSpuelerAndSwitchToView(view, name);
        } else if (validator.isNameToLong(name)) {
            validator.createErrorMessageForTooLongName(currentView);
        } else {
            validator.createErrorMessageForEmptyName(currentView);
        }
        releaseHelper();
    }

    private void createSpuelerAndSwitchToView(View view, String name) {
        spuelerDaoImpl.createSpueler(name, 0);
        switchView(view);
    }

    private String readNameInput() {
        final EditText edit = (EditText) findViewById(R.id.nameInput);
        return (String) edit.getText().toString();
    }

    private void switchView(View view) {
        Intent intent = new Intent(view.getContext(), MainMenu.class);
        startActivity(intent);
    }

    private View getView() {
        return findViewById(R.id.addSpuelerMenu);
    }

    private void releaseHelper() {
        OpenHelperManager.releaseHelper();
    }

}


