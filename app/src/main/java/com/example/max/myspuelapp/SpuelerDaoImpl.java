package com.example.max.myspuelapp;

import android.content.Context;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.stmt.QueryBuilder;

import java.sql.SQLException;
import java.util.List;

public class SpuelerDaoImpl {

  private Context activityContext;

  public SpuelerDaoImpl (Context context) {
    activityContext = context;
  }

  DatabaseHelper dbHelper;
  RuntimeExceptionDao<Spueler, Integer> spuelerDao;

  public List<Spueler> getAllSpueler() {
    return spuelerDao.queryForAll();
  }

  public void initializeDbHelper() {
    dbHelper = (DatabaseHelper) OpenHelperManager.getHelper(activityContext, DatabaseHelper.class);
    spuelerDao = dbHelper.getSpuelerRuntimeExceptionDao();
  }

  public void createSpueler(String name, int score) {
    spuelerDao.create(new Spueler(name, score));
  }

  public Spueler getSpuelerByName(String name) throws SQLException {
    QueryBuilder<Spueler, Integer> queryBuilder = spuelerDao.queryBuilder();
    queryBuilder.where().eq("name", name);
    List<Spueler> spuelerList = queryBuilder.query();
    if(spuelerList.size() > 0) {
      return spuelerList.get(0);
    }
    else return null;
  }

  public int getScoreByName(String name) throws SQLException {
    Spueler spueler = getSpuelerByName(name);
    return spueler.getScore();
  }

  public void addScore(String name, int score) throws SQLException {
    Spueler spueler = getSpuelerByName(name);
    int currentScore = spueler.getScore();
    int newScore = currentScore + score;
    spueler.setScore(newScore);
    spuelerDao.update(spueler);
  }

  public void updateSpueler(Spueler spueler) {
    spuelerDao.update(spueler);
  }

  public void deleteSpuelerByName(String spuelerName) throws SQLException {
    Spueler spueler = getSpuelerByName(spuelerName);
    spuelerDao.delete(spueler);
  }

  public long getHighestScore() {
    long maxScore = spuelerDao.queryRawValue(
            "select max(score) from Spueler");
    return maxScore;
  }

  public boolean canSpuelerBeAdded() {
    long numberOfPlayers = spuelerDao.queryRawValue(
      "select COUNT(*) from Spueler"
    );
    return numberOfPlayers < 8;
  }

}
