package com.example.max.myspuelapp;

import android.support.design.widget.Snackbar;
import android.view.View;

public class Validator {

    final int length = Snackbar.LENGTH_SHORT;

    public void createErrorMessageForEmptyName(View view) {
        String message = "Bitte geben Sie einen Namen an";
        Snackbar.make(view, message, length).show();
    }

    public void createErrorMessageForTooLongName(View view) {
        String message = "Der Name darf höchstens 15 Zeichen betragen";
        Snackbar.make(view, message, length).show();
    }

    public boolean isNameToLong(String name) {
        if (name.length() > 15) {
            return true;
        } else {
            return false;
        }
    }

    public void createErrorMessageForTooBigNumber(View view) {
        String message = "Die eingegebene Zahl ist zu groß";
        Snackbar.make(view, message, length).show();
    }
}
