package com.example.max.myspuelapp;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.j256.ormlite.android.apptools.OpenHelperManager;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.LinkedList;

import static com.j256.ormlite.android.apptools.OpenHelperManager.releaseHelper;

public class MainActivity extends AppCompatActivity {

  SpuelerDaoImpl spuelerDaoImpl = new SpuelerDaoImpl(this);

  HashMap<String, Integer> levels = new HashMap<String, Integer>();
  LinkedList<Integer> lastLevels = new LinkedList<Integer>();

  private void setMap() {
    levels.put("Einfach", 1);
    levels.put("Normal", 2);
    levels.put("Schwer", 3);
    levels.put("Extrem", 4);
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setMap();
    setContentView(R.layout.activity_main);
    TextView textView = (TextView) findViewById(R.id.spuelerName);
    String spuelerName = getSpuelerName();
    textView.setText(spuelerName);
    try {
      readScoreFromDBAndUpdateScoreView(spuelerName);
    } catch (SQLException e) {
      e.printStackTrace();
    }
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    getMenuInflater().inflate(R.menu.menu_main, menu);
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    int id = item.getItemId();
    String spuelerName = getSpuelerName();
    View view = getView();
    switch (id) {
      case R.id.item_option_update:
        Intent updateSpuelerMenu = new Intent(view.getContext(), UpdateSpueler.class);
        updateSpuelerMenu.putExtra("name", spuelerName);
        startActivity(updateSpuelerMenu);
        break;

      case R.id.item_option_delete:
        deleteSpueler(spuelerName);
        finish();
        Intent mainMenu = new Intent(view.getContext(), MainMenu.class);
        startActivity(mainMenu);
        break;
    }

    return true;
  }

  private void deleteSpueler(String spuelerName) {
    spuelerDaoImpl.initializeDbHelper();
    try {
      spuelerDaoImpl.deleteSpuelerByName(spuelerName);
    } catch (SQLException e) {
      e.printStackTrace();
    }
    releaseHelper();
  }

  private void readScoreFromDBAndUpdateScoreView(String name) throws SQLException {
    int score = readScoreFromDatabase(name);
    updateScoreView(score);
  }

  private int getScoreViewScore() {
    TextView scoreView = getScoreView();
    return Integer.parseInt(scoreView.getText().toString());
  }

  private void updateScoreView(int score) {
    TextView scoreView = getScoreView();
    scoreView.setText(Integer.toString(score));
  }

  private int readScoreFromDatabase(String name) throws SQLException {
    spuelerDaoImpl.initializeDbHelper();
    int score = spuelerDaoImpl.getScoreByName(name);
    return score;
  }

  private String getSpuelerName() {
    String spuelerName = "";
    Bundle extras = getIntent().getExtras();
    if (extras != null) {
      spuelerName = extras.getString("name");
    }
    return spuelerName;
  }

  @Override
  public void onBackPressed() {
    readScoreAndAddToDatabase();
    View view = getView();
    Intent intent = new Intent(view.getContext(), MainMenu.class);
    startActivity(intent);
  }

  private void readScoreAndAddToDatabase() {
    spuelerDaoImpl.initializeDbHelper();
    int score = readScore();
    String name = getSpuelerName();
    try {
      spuelerDaoImpl.addScore(name, score);
    } catch (SQLException e) {
      e.printStackTrace();
    }
    releaseHelper();
  }

  private void releaseHelper() {
    OpenHelperManager.releaseHelper();
  }

  public void onLevelClick(View v) {
    Button button = (Button) v;
    String text = button.getText().toString();
    int level = levels.get(text);
    lastLevels.add(level);
    readScoreAndAddToCurrent(level);
  }

  private void readScoreAndAddToCurrent(int level) {
    int currentScore = readScore();
    addToScore(currentScore, level);
  }

  public void onSaveClick(View v) {
    readScoreAndAddToDatabase();
    int spuelgangScore = readScore();
    int score = getScoreViewScore();
    int newScore = spuelgangScore + score;
    updateScoreView(newScore);
    resetScore();
  }

  private TextView getOutput() {
    return (TextView) findViewById(R.id.output);
  }

  private TextView getScoreView() {
    return (TextView) findViewById(R.id.spuelstand);
  }

  private int readScore() {
    TextView textView = getOutput();
    int score = Integer.parseInt(textView.getText().toString());
    return score;
  }

  public void resetScore() {
    TextView textView = getOutput();
    lastLevels.clear();
    textView.setText("0");
  }

  public void onClear(View v) {

    AlertDialog.Builder builder = new AlertDialog.Builder((MainActivity.this));
    builder.setCancelable(true);
    builder.setTitle("Spülstand zurücksetzen");
    builder.setMessage("Soll der aktuelle Spülstand wirklich zurückgesetzt werden?");
    builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialogInterface, int i) {
        dialogInterface.cancel();
      }
    });

    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialogInterface, int i) {
        resetScore();
      }
    });
    builder.show();
  }

  public void resetLast(View v) {
    TextView textView = getOutput();
    int currentScore = readScore();
    if (currentScore > 0) {
      String newScore = String.valueOf(currentScore - lastLevels.getLast());
      if (lastLevels.size() > 0) {
        lastLevels.removeLast();
      }
      textView.setText(newScore);
    }
  }

  private void addToScore(int currentScore, int level) {
    TextView textView = getOutput();
    String newScore = String.valueOf(calculateNewScore(currentScore, level));
    textView.setText(newScore);
  }

  private int calculateNewScore(int currentScore, int level) {
    return currentScore + level;
  }

  private View getView() {
    return findViewById(android.R.id.content);
  }

}
