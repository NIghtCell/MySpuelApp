package com.example.max.myspuelapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.j256.ormlite.android.apptools.OpenHelperManager;

import java.sql.SQLException;

public class UpdateSpueler extends AppCompatActivity {

  SpuelerDaoImpl spuelerDaoImpl = new SpuelerDaoImpl(this);
  Spueler spuelerToUpdate;

  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    try {
      getSpuelerToUpdate();
    } catch (SQLException e) {
      e.printStackTrace();
    }
    setContentView(R.layout.update_spueler);
    getSpuelerDataAndSetToView();
  }

  private void getSpuelerToUpdate() throws SQLException {
    spuelerDaoImpl.initializeDbHelper();
    String oldName = getSpuelerName();
    spuelerToUpdate = spuelerDaoImpl.getSpuelerByName(oldName);
    releaseHelper();
  }

  private void releaseHelper() {
    OpenHelperManager.releaseHelper();
  }

  public void onUpdateSpuelerClick(View view) throws SQLException {
    String newName = readNameInput();
    View currentView = getView();
    try {
      int newScore = readScoreInput();
      validateInputAndUpdateSpueler(view, newName, newScore, currentView);
    } catch (Exception ex) {
      Validator validator = new Validator();
      validator.createErrorMessageForTooBigNumber(currentView);
    }
  }

  private void validateInputAndUpdateSpueler(View view, String newName, int newScore, View currentView) {
    Validator validator = new Validator();
    if (!newName.isEmpty() && !validator.isNameToLong(newName)) {
      updateSpueler(newName, newScore);
      switchToMainMenu(view);
    } else if (validator.isNameToLong(newName)) {
      validator.createErrorMessageForTooLongName(currentView);
    } else {
      validator.createErrorMessageForEmptyName(currentView);
    }
  }

  private void updateSpueler(String newName, int newScore) {
    spuelerDaoImpl.initializeDbHelper();
    spuelerToUpdate.setName(newName);
    spuelerToUpdate.setScore(newScore);
    spuelerDaoImpl.updateSpueler(spuelerToUpdate);
    releaseHelper();
  }

  private String readNameInput() {
    TextView nameInput = findViewById(R.id.updateName);
    return nameInput.getText().toString();
  }

  private int readScoreInput() {
    TextView scoreInput = findViewById(R.id.updateScore);
    String scoreText = scoreInput.getText().toString();
    return Integer.parseInt(scoreText);
  }

  private void getSpuelerDataAndSetToView() {
    spuelerDaoImpl.initializeDbHelper();
    String spuelerName = spuelerToUpdate.getName();
    int score = 0;
    try {
      score = spuelerDaoImpl.getScoreByName(spuelerName);
    } catch (SQLException e) {
      e.printStackTrace();
    }
    setSpuelerDataToView(spuelerName, score);
    releaseHelper();
  }

  private void setSpuelerDataToView(String spuelerName, int score) {
    setNameOfCurrentSpuelerToTextView(spuelerName);
    setScoreOfCurrentSpuelerToTextView(score);
  }

  private void setScoreOfCurrentSpuelerToTextView(int score) {
    TextView updateScore = (TextView) findViewById(R.id.updateScore);
    updateScore.setText(Integer.toString(score));
  }

  private void setNameOfCurrentSpuelerToTextView(String spuelerName) {
    TextView updateName = (TextView) findViewById(R.id.updateName);
    updateName.setText(spuelerName);
  }

  private String getSpuelerName() {
    String spuelerName = "";
    Bundle extras = getIntent().getExtras();
    if (extras != null) {
      spuelerName = extras.getString("name");
    }
    return spuelerName;
  }

  private void switchToMainMenu(View view) {
    Intent intent = new Intent(view.getContext(), MainMenu.class);
    startActivity(intent);
  }

  private View getView() {
    return findViewById(R.id.updateSpuelerMenu);
  }

}
