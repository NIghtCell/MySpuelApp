package com.example.max.myspuelapp;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

public class DatabaseHelper extends OrmLiteSqliteOpenHelper{

  private static final String DATABASE_NAME = "spueler.db";
  private static final int DATABASE_VERSION = 1;

  private Dao<Spueler, Integer> spuelerDao = null;
  private RuntimeExceptionDao<Spueler, Integer> spuelerRuntimeDao = null;

  public DatabaseHelper(Context context){
    super(context, DATABASE_NAME, null, DATABASE_VERSION, R.raw.ormlite_config);
  }

  @Override
  public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {
    try {
      TableUtils.createTable(connectionSource, Spueler.class);
    } catch (SQLException e) {
      e.printStackTrace();
    }
  }

  @Override
  public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource, int oldVersion, int newVersion) {
    try {
      TableUtils.dropTable(connectionSource, Spueler.class, true);
      onCreate(database, connectionSource);
    } catch (SQLException e) {
      e.printStackTrace();
    }
  }

  public Dao<Spueler, Integer> getSpuelerDao() throws SQLException {

    if(spuelerDao == null) {
      spuelerDao = getDao(Spueler.class);
    }
    return spuelerDao;
  }

  public RuntimeExceptionDao<Spueler, Integer> getSpuelerRuntimeExceptionDao() {
    if(spuelerRuntimeDao == null) {
      spuelerRuntimeDao = getRuntimeExceptionDao(Spueler.class);
    }
    return spuelerRuntimeDao;
  }
}
