package com.example.max.myspuelapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.j256.ormlite.android.apptools.OpenHelperManager;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class MainMenu extends AppCompatActivity {

  SpuelerDaoImpl spuelerdaoImpl = new SpuelerDaoImpl(this);

  @Override
  public void onBackPressed() {}

  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.main_menu);
    spuelerdaoImpl.initializeDbHelper();
    List<Spueler> spueler = spuelerdaoImpl.getAllSpueler();
    ArrayList<Button> buttons = addSpuelerButtons(spueler);
    initButtonListeners(buttons);
    releaseHelper();
  }

  private ArrayList<Button> addSpuelerButtons(List<Spueler> spueler) {
    ArrayList<Button> buttons = new ArrayList<>();
    int scoreDifference = 0;
    for (Spueler spl : spueler) {
      scoreDifference = getScoreDifference(scoreDifference, spl);
      String buttonText = createButtonTextWithScoreDifference(scoreDifference, spl);
      buttons.add(addButtonToMainMenu(buttonText));
    }
    return buttons;
  }

  private int getScoreDifference(int scoreDifference, Spueler spl) {
    try {
      scoreDifference = getDifferenceFromHighestScore(spl.getScore());
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return scoreDifference;
  }

  private String createButtonTextWithScoreDifference(int scoreDifference, Spueler spl) {
    String buttonText = spl.getName();
    if(scoreDifference != 0) {
      buttonText += " (-" + scoreDifference + ")";
    }
    return buttonText;
  }

  private int getDifferenceFromHighestScore(int score) throws SQLException {
    spuelerdaoImpl.initializeDbHelper();
    int highScore = (int) spuelerdaoImpl.getHighestScore();
    releaseHelper();
    return highScore - score;
  }

  private void releaseHelper() {
    OpenHelperManager.releaseHelper();
  }

  private Button addButtonToMainMenu(String buttonText) {

    LinearLayout linearLayout = (LinearLayout) findViewById(R.id.main_menu);

    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.WRAP_CONTENT,
            LinearLayout.LayoutParams.WRAP_CONTENT);

    linearLayout.setOrientation(LinearLayout.VERTICAL);

    Button button = new Button(this);
    button.setText(buttonText);
    button.setLayoutParams(params);
    button.setGravity(Gravity.CENTER_HORIZONTAL);

    linearLayout.addView(button);

    return button;
  }

  private void initButtonListeners(ArrayList<Button> buttons) {
    for (Button button : buttons) {
      initButtonListener(button);
    }
    initAddSpuelerListener();
  }

  private void initButtonListener(final Button button) {
    button.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
          startMainActivityWithPlayerName(view, button);
      }
    });
  }

  private void showErrorMessage() {
    View view = getView();
    int duration = Snackbar.LENGTH_SHORT;
    String message = "Es können nicht mehr als 8 " +
            "Spüler auf einmal angelegt werden";
    Snackbar.make(view, message, duration).show();
  }

  private void startMainActivityWithPlayerName(View view, Button button) {
    String buttonName = getButtonName(button);
    Intent intent = new Intent(view.getContext(), MainActivity.class);
    intent.putExtra("name", buttonName);
    startActivity(intent);
  }

  private View getView() {
    return findViewById(R.id.main_menu);
  }

  private void initAddSpuelerListener() {
    Button button = (Button) findViewById(R.id.addSpueler);
    button.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        spuelerdaoImpl.initializeDbHelper();
        if(spuelerdaoImpl.canSpuelerBeAdded()) {
          Intent intent = new Intent(view.getContext(), AddSpueler.class);
          startActivity(intent);
        } else {
          showErrorMessage();
        }
        releaseHelper();
      }
    });

  }

  private String getButtonName(Button button) {
    String buttonText = getButtonText(button);
    String buttonName;
    try {
      buttonName = buttonText.substring(0, buttonText.indexOf(' '));
    } catch (StringIndexOutOfBoundsException ex) {
      buttonName = buttonText;
    }

    return buttonName;
  }

  private String getButtonText(Button button) {
    String buttonText = button.getText().toString();
    return buttonText;
  }


}


