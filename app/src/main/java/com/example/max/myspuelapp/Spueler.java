package com.example.max.myspuelapp;

import com.j256.ormlite.field.DatabaseField;

public class Spueler {

  @DatabaseField(generatedId = true)
  int id;

  @DatabaseField
  String name;

  @DatabaseField
  int score;

  public Spueler(String name, int score) {
    super();
    this.name = name;
    this.score = score;
  }

  public Spueler() {

  }

  public void setName(String name) {
    this.name = name;
  }

  public int getScore() {
    return score;
  }

  public void setScore(int score) {
    this.score = score;
  }

  public String getName() {
   return this.name;
  }

}
